# -*- coding: utf-8 -*-
import dataset
import logging
import OAuth2Util
import praw
import time
import tweepy
import warnings

from StTwitterBot import stTwitterOAuth

# logging
logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s] %(message)s")
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

# Database connection
db = dataset.connect("sqlite:///twitterbot.db")
tweeted_subs = db["tweeted"]

SUBREDDIT = "Moduniverse"
USER_AGENT = "Reddit->Twitter script for /r/{} by /u/efflicto v 0.0.1".format(SUBREDDIT)

# Your conditions
KARMA_TRESHOLD = 0
APPROVEDCHECK = True

LIMIT = 100


def db_tweeted(submission_id):
    """
    Writes the submission id in the database
    :param submission_id:
    :return:
    """
    tweeted_subs.insert(dict(
        submission_id=submission_id
    ))


def db_is_tweeted(submission_id):
    """
    Checks if the submission id is in the database
    :param submission_id:
    :return:
    """
    if tweeted_subs.find_one(submission_id=submission_id):
        return True
    else:
        return False


def get_reddit_api():
    """
    Generates a reddit API object
    :return:
    """
    try:
        logger.debug("Getting reddit session and OAuth.")
        r = praw.Reddit(user_agent=USER_AGENT)
        o = OAuth2Util.OAuth2Util(r)
        o.refresh()
        return r
    except Exception as e:
        logger.exception(e)


def get_twitter_api():
    """
    Generates a twitter API object
    :return:
    """
    logger.debug("Getting Twitter session and OAuth.")
    try:
        auth = tweepy.OAuthHandler(
            consumer_key=stTwitterOAuth.consumer_key, consumer_secret=stTwitterOAuth.consumer_secret
        )
        auth.set_access_token(key=stTwitterOAuth.access_token, secret=stTwitterOAuth.access_token_secret)
        api = tweepy.API(auth_handler=auth, wait_on_rate_limit=True)  # We'll wait for the rate limit to be save.
        return api
    except Exception as e:
        logger.exception(e)


def do_tweet(api, tweet):
    """
    Tweets out the given text
    :param api:
    :param tweet:
    :return:
    """
    try:
        api.update_status(status=tweet)
    except Exception as e:
        logger.exception(e)


def tweet_factory(submission):
    """
    Factorizes a submission title to the given rules
    Returns None if tweet is too long
    :param submission:
    :return:
    """
    try:
        submission_len = len(submission.title)
        if submission_len <= 99:
            return "{} #{} {}".format(submission.title, SUBREDDIT, submission.short_link)
        if 100 <= submission_len <= 116:
            return "{} {}".format(submission.title, submission.short_link)
        if 117 <= submission_len <= 124:
            return "{} #{}".format(submission.title, SUBREDDIT)
        if 125 <= submission_len <= 140:
            return "{}".format(submission.title)
        if submission_len > 140:
            logger.debug("Subission is {} long. Not valid!".format(submission_len))
            return None
    except Exception as e:
        logger.exception(e)
        return None


def get_valid_submissions(reddit_api):
    """
    Crawls subreddit submissions with the given conditions and returns them as a list of reddit submission objects
    :param reddit_api:
    :return:
    """
    try:
        valid_submissions = []
        subreddit = reddit_api.get_subreddit(SUBREDDIT)
        submissions = subreddit.get_hot(limit=LIMIT)
        for submission in submissions:
            if not db_is_tweeted(submission.id):
                if APPROVEDCHECK:
                    if submission.approved_by is not None:
                        if submission.score > KARMA_TRESHOLD:
                            if len(submission.title) <= 140:
                                logger.debug("Valid submission: {}|{}|Approved:{}".format(
                                    submission.title,
                                    len(submission.title),
                                    submission.approved_by))
                                valid_submissions.append(submission)
                else:
                    if submission.score > KARMA_TRESHOLD:
                        if len(submission.title) <= 140:
                            logger.debug("Valid submission: {}|{}|Approved:{}".format(
                                submission.title,
                                len(submission.title),
                                submission.approved_by))
                            valid_submissions.append(submission)
        return valid_submissions
    except Exception as e:
        logger.exception(e)


def main():
    """
    Main method to process everything
    :return:
    """
    # Ignore any tweepy warnings...
    warnings.filterwarnings("ignore")
    # Timer for time measurement
    start_time = time.time()
    # Get the APIs
    twitter_api = get_twitter_api()
    reddit_api = get_reddit_api()
    # Get all valid submissions
    logger.info("Crawling valid submission")
    submissions = get_valid_submissions(reddit_api=reddit_api)
    logger.info("Got {} valid submissions".format(len(submissions)))
    # Iterate through every submission and factorize them
    for submission in submissions:
        tweet = tweet_factory(submission)
        if tweet:
            if not db_is_tweeted(submission_id=submission.id):
                db_tweeted(submission.id)
                do_tweet(api=twitter_api, tweet=tweet)
                logger.debug("Tweeted: {} {}".format(tweet, len(tweet)))
    logger.info("All done in {} seconds.".format(round((time.time() - start_time), 2)))

if __name__ == "__main__":
    main()
