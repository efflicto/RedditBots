# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import praw
import OAuth2Util

USER = "username"  # your username
USER_AGENT = "Flair message app by /u/efflicto written for /u/{}".format(USER)
SUB = "pcmasterrace"  # your sub to watch
FLAIR = "Peasantry"  # your flair keyword to watch

r = praw.Reddit(user_agent=USER_AGENT)
o = OAuth2Util.OAuth2Util(r)
o.refresh()

submissions = r.get_subreddit(SUB).get_new(limit=100)  # get the last 100 new submissions

subject = "Submissions flaired with {} from sub {}.".format(FLAIR, SUB)
message = ""

for submission in submissions:
    if submission.link_flair_text is not None:
        if FLAIR in submission.link_flair_text:
            message += submission.short_link+"\n\n"


r.send_message(recipient=USER, subject=subject, message=message)
