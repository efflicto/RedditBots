# -*- coding: utf-8 -*-
import dataset
import facebook
import logging
import OAuth2Util
import praw
import time

from StFBBot import stFbBotConfig

# logging
logger = logging.getLogger("logger")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s] %(message)s")
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

# Database connection
db = dataset.connect("sqlite:///facebookbot.db")
posted_subs = db["fbposted"]

SUBREDDIT = "Moduniverse"
USER_AGENT = "Reddit->Facebook script for /r/{} by /u/efflicto v 0.0.1".format(SUBREDDIT)

# Your conditions
KARMA_TRESHOLD = 0
APPROVEDCHECK = False

LIMIT = 100


def db_posted(submission_id):
    """
    Writes the submission id in the database
    :param submission_id:
    :return:
    """
    posted_subs.insert(dict(
        submission_id=submission_id
    ))


def db_is_posted(submission_id):
    """
    Checks if the submission id is in the database
    :param submission_id:
    :return:
    """
    if posted_subs.find_one(submission_id=submission_id):
        return True
    else:
        return False


def get_reddit_api():
    """
    Generates a reddit API object
    :return:
    """
    try:
        logger.debug("Getting reddit session and OAuth.")
        r = praw.Reddit(user_agent=USER_AGENT)
        o = OAuth2Util.OAuth2Util(r)
        o.refresh()
        return r
    except Exception as e:
        logger.exception(e)


def get_fb_api():
    """
    Returns the Facebook graph API
    :return:
    """
    try:
        logger.info("Getting FB API")
        if stFbBotConfig.fb_page_access_token and stFbBotConfig.fb_page_access_token is not "":
            graph = facebook.GraphAPI(access_token=stFbBotConfig.fb_page_access_token)
            return graph
        else:
            raise ValueError("fb_page_access_token is not set. Please edit your config.")
    except Exception as e:
        logger.exception(e)
        return None


def do_fb_post(api, post):
    """
    Makes a wall post to Facebook
    :param api:
    :param post:
    :return:
    """
    try:
        logger.debug("Posting: {}".format(post))
        api.put_wall_post(post)
    except Exception as e:
        logger.exception(e)


def get_valid_submissions(reddit_api):
    """
    Crawls subreddit submissions with the given conditions and returns them as a list of reddit submission objects
    :param reddit_api:
    :return:
    """
    try:
        valid_submissions = []
        subreddit = reddit_api.get_subreddit(SUBREDDIT)
        submissions = subreddit.get_hot(limit=LIMIT)
        for submission in submissions:
            if not db_is_posted(submission.id):
                if APPROVEDCHECK:
                    if submission.approved_by is not None:
                        if submission.score > KARMA_TRESHOLD:
                            logger.debug("Valid submission: {}|{}|Approved:{}".format(
                                submission.title,
                                len(submission.title),
                                submission.approved_by))
                            valid_submissions.append(submission)
                else:
                    if submission.score > KARMA_TRESHOLD:
                        logger.debug("Valid submission: {}|{}|Approved:{}".format(
                            submission.title,
                            len(submission.title),
                            submission.approved_by))
                        valid_submissions.append(submission)
        return valid_submissions
    except Exception as e:
        logger.exception(e)


def main():
    """
    Main method to process everything
    :return:
    """
    # Timer for time measurement
    start_time = time.time()
    # Get the APIs
    fb_api = get_fb_api()
    reddit_api = get_reddit_api()

    logger.info("Crawling valid submission")
    submissions = get_valid_submissions(reddit_api=reddit_api)
    logger.info("Got {} valid submissions".format(len(submissions)))
    # Iterate through every submission and factorize them
    for submission in submissions:
        post = "{}\n#{}\n{}".format(submission.title, SUBREDDIT, submission.short_link)
        db_posted(submission.id)
        do_fb_post(api=fb_api, post=post)
        logger.debug("Posted: {}".format(post))

    logger.info("All done in {} seconds.".format(round((time.time() - start_time), 2)))


if __name__ == "__main__":
    main()
