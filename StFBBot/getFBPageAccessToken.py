# -*- coding: utf-8 -*-
import facebook
from StFBBot import stFbBotConfig


def main():
    graph = facebook.GraphAPI(stFbBotConfig.fb_access_token)
    resp = graph.get_object('me/accounts')
    page_access_token = None
    for page in resp['data']:
        if page['id'] == stFbBotConfig.fb_page_id:
            page_access_token = page['access_token']
    print("Here is your page access token. Please fill it into your stFbBotConfig")
    print(page_access_token)


if __name__ == "__main__":
    main()
