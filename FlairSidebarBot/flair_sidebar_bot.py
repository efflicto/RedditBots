# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import datetime
import logging
import OAuth2Util
import praw
import sys
import time

# your username/bot name
USER = "efflicto"
USER_AGENT = "Flair message app by /u/efflicto written for /u/{}".format(USER)

# your sub to watch
SUB = "efflicto_bot"
WIKI_PAGE = "sidebar_template"

LIMIT = 100

# Tags in your wiki page
START_TAG = "[](#Start)"
END_TAG = "[](#End)"

# Flairs to watch
# "Flair name": "Plural"
FLAIRS = {
    "Giveaway": "Giveaways",
    "Contest": "Contests"
}


def main():
    start_time = time.time()
    logger = logging.getLogger("logger")
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s] %(message)s")
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    try:
        logger.info("Getting reddit session and OAuth.")
        r = praw.Reddit(user_agent=USER_AGENT)
        o = OAuth2Util.OAuth2Util(r)
        o.refresh()
        logger.info("Getting subreddit, submissions and wiki page.")
        subreddit = r.get_subreddit(SUB)
        submissions = subreddit.get_new(limit=LIMIT)
        wiki_page = r.get_wiki_page(SUB, WIKI_PAGE)

        wiki_start = wiki_page.content_md.split(START_TAG)[0]
        wiki_end = wiki_page.content_md.split(END_TAG)[1]
        flair_links = []
        valid_submissions = []
        logger.info("Crawling for valid submissions.")
        for submission in submissions:
            if submission.link_flair_text is not None:
                if submission.link_flair_text in FLAIRS:
                    valid_submissions.append(submission)
        logger.info("Got {} valid submissions".format(len(valid_submissions)))

        logger.info("Going through all submissions.")

        for singular, plural in FLAIRS.items():
            sub_len = 0
            flair_text = ""
            flair_text += "{}\n\n".format(plural)
            flair_text += "User | Comments | Link\n"
            flair_text += "---|---|---\n"
            for submission in valid_submissions:
                if submission.link_flair_text is not None:
                    if singular in submission.link_flair_text:
                        author = "/u/{}".format(submission.author)
                        flair_text += "{} | {} | {}".format(author, submission.num_comments, submission.short_link)
                        flair_text += "\n"
                        sub_len += 1
            flair_text += "\n\n"
            if sub_len > 0:
                flair_links.append(flair_text)
                logger.info("Got {} submissions for {}. ".format(sub_len, singular))
            else:
                flair_text = "{}\n\n".format(plural)
                flair_text += "There are no {} currently.\n\n".format(plural.lower())
                flair_links.append(flair_text)
                logger.info("Got no submissions for {}.".format(singular))

        logger.info("Generating wiki template.")
        wiki_add = "".join(flair_links)
        wiki_full = "{}\n\n{}{}\n\n{}{}".format(wiki_start, START_TAG, wiki_add, END_TAG, wiki_end)
        logger.info("Updating wiki page {}.".format(WIKI_PAGE))
        r.edit_wiki_page(
            subreddit=SUB,
            page=WIKI_PAGE,
            content=wiki_full,
            reason="Edited by {} on {}".format(
                USER,
                datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
            )
        )
        logger.info("All done in {} seconds.".format(round((time.time()-start_time), 2)))

    except KeyboardInterrupt:
        logger.info("Keyboard interrupt. Exiting.")
        sys.exit()
    except Exception as e:
        logger.error("Error.")
        logger.exception(e)


if __name__ == "__main__":
    main()
